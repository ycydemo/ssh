package com.it.action;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.it.bean.Customer;
import com.it.service.CustomerService;
import com.it.serviceimpl.CustomerServiceImpl;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
@Controller("customerAction")//一般用于表现层对象(Action)
@Scope("prototype")//默认是单例模式，即scope="singleton"。scope="prototype"多例
@ParentPackage(value="struts-default")//对应xml配置文件中的package的父包，一般需要继承struts-default
@Namespace(value="/")//对应配置文件中的nameSpace，命名空间
public class CustomerAction extends ActionSupport implements ModelDriven<Customer> {
	// 模型封装
	private Customer custmoer = new Customer();

	@Override
	public Customer getModel() {
		// TODO Auto-generated method stub
		return custmoer;
	}

	// 注入CustomerService:
	@Autowired
	private CustomerService customerService;
	
	// 保存客户的方法
	@Action(value="customer_save")
	public String save() {
		System.out.println("action执行了");
		System.out.println(custmoer);
		customerService.save(custmoer);
		return null;
	}

}
