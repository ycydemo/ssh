package com.it.daoimpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.it.bean.Customer;
import com.it.dao.CustomerDao;
//使用注解注入dao
@Repository("customerDao")//一般用于持久对象(dao)
public class CustomerDaoImpl  implements CustomerDao {
	@Autowired
	private HibernateTemplate hibernateTemplate;
	

	@Override
	public void save(Customer custmoer) {
		// TODO Auto-generated method stub
		System.out.println("dao save");
		this.hibernateTemplate.save(custmoer);
	}

}
