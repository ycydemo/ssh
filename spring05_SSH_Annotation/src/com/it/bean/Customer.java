package com.it.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//hibernate持久化类
@Entity
//表名
@Table(name="cst_customer")
public class Customer {
	//设置id
	@Id
	//这个属性在表中的名称
	@Column(name="cust_id")
	//设置自增
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int cust_id;// '客户编号(主键)',
	@Column(name="cust_name")
	private String cust_name;//'客户名称(公司名称)',
	@Column(name="cust_source")
	private String cust_source;// '客户信息来源',
	@Column(name="cust_industry")
	private String cust_industry;// '客户所属行业',
	@Column(name="cust_level")
	private String cust_level;// '客户级别',
	@Column(name="cust_phone")
	private String cust_phone;//'固定电话',
	@Column(name="cust_mobile")
	private String cust_mobile;//'移动电话',
	
	public int getCust_id() {
		return cust_id;
	}

	public void setCust_id(int cust_id) {
		this.cust_id = cust_id;
	}

	public String getCust_name() {
		return cust_name;
	}

	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}

	public String getCust_source() {
		return cust_source;
	}

	public void setCust_source(String cust_source) {
		this.cust_source = cust_source;
	}

	public String getCust_industry() {
		return cust_industry;
	}

	public void setCust_industry(String cust_industry) {
		this.cust_industry = cust_industry;
	}

	public String getCust_level() {
		return cust_level;
	}

	public void setCust_level(String cust_level) {
		this.cust_level = cust_level;
	}

	public String getCust_phone() {
		return cust_phone;
	}

	public void setCust_phone(String cust_phone) {
		this.cust_phone = cust_phone;
	}

	public String getCust_mobile() {
		return cust_mobile;
	}

	public void setCust_mobile(String cust_mobile) {
		this.cust_mobile = cust_mobile;
	}

	@Override
	public String toString() {
		return "Customer [cust_id=" + cust_id + ", cust_name=" + cust_name + ", cust_source=" + cust_source
				+ ", cust_industry=" + cust_industry + ", cust_level=" + cust_level + ", cust_phone=" + cust_phone
				+ ", cust_mobile=" + cust_mobile + "]";
	}
	
}
