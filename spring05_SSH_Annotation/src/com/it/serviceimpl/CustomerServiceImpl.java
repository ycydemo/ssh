package com.it.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.it.bean.Customer;
import com.it.dao.CustomerDao;
import com.it.daoimpl.CustomerDaoImpl;
import com.it.service.CustomerService;
@Service("customerService")//一般用于业务层对象(service)
public class CustomerServiceImpl implements CustomerService{
	@Autowired
	private CustomerDao customerDao;
	
	@Override
	@Transactional
	public void save(Customer custmoer) {
		// TODO Auto-generated method stub
		System.out.println("service save");
		customerDao.save(custmoer);
	}
	
}
